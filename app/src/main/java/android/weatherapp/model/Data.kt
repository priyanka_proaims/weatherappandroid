data class Data (

	 val request : List<Request>,
	 val current_condition : List<Current_condition>,
	 val weather : List<Weather>,
	 val climateAverages : List<ClimateAverages>
)