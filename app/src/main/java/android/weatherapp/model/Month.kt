
data class Month (

	 val index : Int,
	 val name : String,
	 val avgMinTemp : Double,
	 val avgMinTemp_F : Double,
	 val absMaxTemp : Double,
	 val absMaxTemp_F : Double,
	 val avgDailyRainfall : Double
)