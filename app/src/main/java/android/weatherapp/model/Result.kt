
data class Result (

	 val areaName : List<AreaName>,
	 val country : List<Country>,
	 val region : List<Region>,
	 val latitude : Double,
	 val longitude : Double,
	 val population : Int,
	 val weatherUrl : List<WeatherUrl>
)